import XCTest
@testable import MyProgressManager

final class MyProgressManagerTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(MyProgressManager().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
