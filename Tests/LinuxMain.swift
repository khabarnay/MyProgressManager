import XCTest

import MyProgressManagerTests

var tests = [XCTestCaseEntry]()
tests += MyProgressManagerTests.allTests()
XCTMain(tests)
